from django.contrib import admin
from booksapp.models import Author, Book, BookReview
from booksapp.models import Magazine, Issue, Genre
# Register your models here.

admin.site.register(Book)
admin.site.register(BookReview)
admin.site.register(Author)
admin.site.register(Magazine)
admin.site.register(Issue)
admin.site.register(Genre)



