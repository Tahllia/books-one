from django.db import models

class Author(models.Model):
    name = models.CharField(max_length=200, unique=True)
    
    def __str__(self):
        return self.name

class Book(models.Model):
    title = models.CharField(max_length=200, unique=True)
    authors = models.ManyToManyField(Author, related_name="books")
    pages = models.SmallIntegerField(null=True)
    isbn = models.BigIntegerField(null=True)
    year_published = models.SmallIntegerField(null=True)
    description = models.TextField(null=True)
    image = models.URLField(null=True, blank=True)
    in_print = models.BooleanField(null=True)

    def __str__(self):
        return self.title + " by " + str(self.authors.first())


class BookReview(models.Model):
    book = models.ForeignKey(Book, related_name="reviews", on_delete=models.CASCADE)
    text = models.TextField()

    def __str__(self):
        return self.text




class Magazine(models.Model):
    title = models.CharField(max_length=50)
    cover_img = models.URLField(null=True, blank=True)
    description = models.TextField(max_length=300)
    release_cycle = models.TextField(blank=True, null=True)
    pages = models.SmallIntegerField(null=True)
    genres = models.ManyToManyField("Genre", related_name="magazines")

    def __str__(self):
        return self.title

class Issue(models.Model):
    magazine = models.ForeignKey(Magazine, related_name="issues", on_delete=models.CASCADE, null=True)
    issue_title = models.CharField(max_length=200, blank=True, null=True)
    date_published = models.SmallIntegerField(null=True, blank=True)
    page_count = models.SmallIntegerField(null=True, blank=True)
    issue_number = models.SmallIntegerField(null=True, blank=True)
    cover_issue_image = models.URLField(null=True, blank=True)
    description = description = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.issue_title + ":" + str(self.issue_number)
        #return self.magazine.title #, ":" , self.issue_number


class Genre(models.Model):
    name = models.CharField(max_length=200)
    def __str__(self):
        return self.name

