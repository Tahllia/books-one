from ast import Delete
from django.contrib import admin
from django.urls import path
from booksapp.views import show_genre, show_mags, add_mag, edit_mag, mags_detail

urlpatterns = [
    path("mag_list/", show_mags, name="show_mags"),
    path("add_mag/", add_mag, name="add_mag"),
    path("mag_list/<int:pk>/", mags_detail, name="mag_detail"),
    path("<int:pk>/edit", edit_mag, name="edit_mag"),
    path("genres/", show_genre, name="show_genre"),

]
