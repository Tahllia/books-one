
from django import forms
from .models import Book
from .models import Magazine
from .models import Author
from .models import BookReview
from .models import Issue
from .models import Genre
# creating a form

class BookForm(forms.ModelForm):

    # create meta class
    class Meta:
        # specify model to be used
        model = Book
# you can also use: exclude = [] to include all the attributes from the class
        # specify fields to be used
        fields = [
            "title",
            "description",
            "authors",
            "image",
        ]

class MagForm(forms.ModelForm):
    class Meta:
        model = Magazine
        fields = [
            "title",
            "description",
            "cover_img",
            "pages",
            "release_cycle",
        ]
