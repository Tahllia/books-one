from ast import Delete
from django.contrib import admin
from django.urls import path
from booksapp.views import add_book, book_detail, show_books, edit_book, delete_book


urlpatterns = [
    path("books_list/", show_books, name="show_books"),
    path("new_book/", add_book, name= "add_a_book"),
    path("<int:pk>/", book_detail, name="book_detail"),
    path("<int:pk>/edit_book", edit_book, name = "edit_a_book"),
    path("<int:pk>/delete", delete_book, name = "delete_book"),
]