from multiprocessing import context
from django.shortcuts import redirect, render, get_object_or_404
from booksapp.models import Book, Magazine, Genre
from .forms import BookForm, MagForm


# Create your views here.

def show_books(request):
    books = Book.objects.all()
    context = {
        "books": books
    }
    return render(request, "books/list.html", context)


def add_book(request):
    context = {}
    form = BookForm(request.POST or None)
    if form.is_valid():
        book = form.save()
        return redirect("book_detail", pk=book.pk)
    context["form"] = form
    return render(request, "books/new_book.html", context)


def book_detail(request, pk):
    context = {
        "book": Book.objects.get(pk=pk)
        # if Book else [],
    }
    return render(request, "books/details.html", context)

# Need to change this im pretty sure


def edit_book(request, pk):
    context = {}
    obj = get_object_or_404(Book, pk=pk)
    form = BookForm(request.POST or None, instance=obj)
    if form.is_valid():
        book = form.save()
        return redirect("book_detail", pk=book.pk)
    context["form"] = form
    return render(request, "books/edit_book.html", context)

def delete_book(request, pk):
        context = {}
        obj = get_object_or_404(Book, pk=pk)
        if request.method == "POST":
            obj.delete()
            return redirect("show_books")
        return render(request, "books/delete_book.html", context)


def show_mags(request):
    mag = Magazine.objects.all()
    context = {
        "magazines": mag
    }
    return render(request, "magazines/mag_list.html", context)

def add_mag(request):
    context = {}
    form = MagForm(request.POST or None)
    if form.is_valid():
        magazine = form.save()
        return redirect("mag_detail", pk=magazine.pk)
    context["form"] = form
    return render(request, "magazines/add_magazines.html", context)


def mags_detail(request, pk):
    magazine = Magazine.objects.get(pk=pk)
    context = {
    "magazine": magazine
}
    return render(request, "magazines/mag_details.html", context)

def edit_mag(request, pk):
    context = {}
    obj = get_object_or_404(Magazine, pk=pk)
    form = MagForm(request.POST or None, instance=obj)
    if form.is_valid():
        mag_form = form.save()
        return redirect("mag_detail", pk=mag_form.pk)
    context["form"] = form
    return render(request, "magazines/edit_mag.html", context)

def show_genre(request):
    genre= Genre.objects.all()
    context={
        "genre": genre
    }
    return render(request, "magazines/genre_view.html", context)
